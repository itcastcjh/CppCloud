package provider

import (
	. "cppcloud"
	"math/rand"
)

// 创建HTTP服务提供者注册操作，具体http路径业务请另外实现

type HTTPProvider struct {
	*ProviderBase
}

// CreateHTTPProvider 创建入口1: 产生HTTPProvider对象，再设置其属性，最后调用Start()执行注册
// port: 当为0时，用随机端口
// host: 可以是域名或IP，当为空时，用cloudapp socker ip
// 注意，如果所连接的Serv和本服务不同网段时（比如Serv在外网）需要调用SetUrl()显式给出服务地址
func CreateHTTPProvider(cloudapp *CloudApp, host string, port int) (pvrd *HTTPProvider) {
	pbase := CreateProviderBase(cloudapp)
	pbase.SetRegName(cloudapp.SvrName())
	pbase.SetScheme("http")
	pbase.SetHost(host)

	if 0 == port {
		port = 1024 + rand.Intn(1000)
	}
	pbase.SetPort(port)

	if "" == host {
		host = cloudapp.CliIP()
		pbase.SetHost(host)
	}

	pvrd = &HTTPProvider{pbase}
	cloudapp.AddNotifyCallBack("shutdown", pvrd.onAppShutdown)
	return
}

func (pvrd *HTTPProvider) Start() {
	pvrd.Regist(true)
}

func (pvrd *HTTPProvider) onAppShutdown(param map[string]interface{}) (code int, result interface{}) {
	return 0, nil
}
